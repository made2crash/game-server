module.exports = {
    PORT: process.env.GS_PORT || 0,
    USE_HTTPS: process.env.USE_HTTPS,
    HTTPS_KEY: process.env.HTTPS_KEY || '',
    HTTPS_CERT: process.env.HTTPS_CERT || '',
    HTTPS_CA: process.env.HTTPS_CA,
    DATABASE_URL:  process.env.DATABASE_URL || "postgres://localhost:5432/bustabitdb",
    ENC_KEY: process.env.ENC_KEY || 'devkey',
    PRODUCTION: process.env.NODE_ENV  === 'production',
    GS_BANKROLL: process.env.GS_BANKROLL  || 0,
    CRASH_AT: process.env.CRASH_AT //Force the crash point
};
