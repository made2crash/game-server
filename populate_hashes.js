var async = require('async');
var db = require('./server/database');
var lib = require('./server/lib');
var _ = require('lodash');
var readline = require('readline');

var offset = 0;

var games = process.env.NUMBER_OF_GAMES || 1e4;
var game = games;
var serverSeed = process.env.SERVER_SEED || 'DO NOT USE THIS SEED';
String(serverSeed);

function loop(cb) {
    var parallel = Math.min(game, 1000);

    var inserts = _.range(parallel).map(function() {

        return function(cb) {
            serverSeed = lib.genGameHash(serverSeed);
            game--;

            db.query('INSERT INTO game_hashes(game_id, hash) VALUES($1, $2)', [offset + game, serverSeed], cb);
        };
    });

    async.parallel(inserts, function(err) {
        if (err) throw err;

        // Clear the current line and move to the beginning.
        var pct = 100 * (games - game) / games;
        readline.clearLine(process.stdout, 0);
        readline.cursorTo(process.stdout, 0, null);
        process.stdout.write(
            "Processed: " + (games - game) + ' / ' + games +
                ' (' + pct.toFixed(2)  + '%)');

        if (game > 0)
            loop(cb);
        else {
            console.log(' Done');
            cb();
        }
    });
}

loop(function() {

    console.log(`Finished at the ${games}th game || with serverSeed : ${serverSeed}`);

});